module.exports = {
  qa: [
    {
      type: 'category',
      label: 'QA Documentation',
      items: [
        'getting-started',
        'test-automation',
      ],
    },
  ],
};
