---
title: Template Basic Page
---

## Template Basic Page

This is your first document in **Docusaurus**, Congratulations!

![Docusaurus logo](/img/docusaurus.png)

Relative Path Link [Create a page](docusaurus/create-a-page.md)

Absolute Path Link [Basic page template](/docs-help/template-basic-page.md)

### Example of React inside your page

export const Highlight = ({children, color}) => (
<span style={{
    backgroundColor: color,
    borderRadius: '2px',
    color: '#fff',
    padding: '0.2rem',
}}>
{children}
</span>
);

<Highlight color="#25c2a0">Docusaurus green</Highlight> and <Highlight color="#1877F2">Facebook blue</Highlight> are my favorite colors.

You can write **Markdown** alongside _JSX_!
