---
title: Main Menu
---

## Creating New Main Menu Item with SideBar

Live reloading is not as reliable when making structural changes.
If in doubt, stop the server and run `npm run build` before restarting with `npm start`

### Step 1 : Add new menu item

Edit `docusaurus.config.js` and under the `items` array add your new menu item.

```
    {
      to: 'docs-example/',
      activeBasePath: 'docs-example',
      label: 'Example',
      position: 'left',
    },
```

### Step 2 : Add new docs folder 

Create a sub-folder `docs-example`

### Step 3 : Create landing page

Create landing page with the `slug: /` so it gets rendered on the menu selection

e.g. File `docs-example/gettting-started.md`

```markdown
---
title: Getting Started
slug: /
---

## Coming Soon...

```

### Step 4 : Create Sidebar Navigation Menu

Create `sidebar-example.js` alongside the others.

```javascript
module.exports = {
  example: [
    {
      type: 'category',
      label: 'Example',
      items: [
          'getting-started'
      ],
    },
  ],
};
```

### Step 5 : Connect Document Generation and Sidebar

Edit `docusaurus.config.js` and under the `plugins` array add your new menu item.

```javascript
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'frontend',
        path: 'docs-frontend',
        routeBasePath: 'docs-frontend',
        sidebarPath: require.resolve('./sidebars-frontend.js'),
        // ... other options
      },
    ],
```
