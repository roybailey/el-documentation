# Docusaurus Engineering Site Template

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

> Status:  _Under Construction / Experimental_

## Motivation

**Use a Static Site Generator for the creation and ongoing maintenance of a gh-pages technical documentation site.**

Mostly interested to avoid installing software that may be difficult or prohibited at large enterprises.
Docusaurus was chosen as a self-contained JavaScript framework that is focused on building great documentation sites.

## Design

**Replace this with one or more paragraphs explaining the design choices and any reasoning, limitations, trade-offs etc.**

It should cover at a high-level approach to testing, security concerns, deployment, disaster recovery criticality.  Picture speaks a thousand words.


## Getting started

```console
npm install
```

### Local Development

```console
npm start
```

This command starts a local development server and open up a browser window.
Most changes are reflected live without having to restart the server.

### Build

```console
npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

TODO:  Add gitlab pipeline to publish site

### Prerequisites


### Key Configuration

Replace this with details of where key configuration files are stored in the repository and what purpose they have.  Include a table of anything that is commonly changed between environments that might be helpful for new starter (e.g. main database properties)

Name                   | Description
 --------------------- | ------------- 
`docusaurus.config`    | contains the docusaurus configuration for the website inc. meta data, title, baseUrl etc.


## Handover Suggestions

_Nuggets of Knowledge and Thinking from last people to work on the project._
_e.g. suggestions for technical debt reduction, simplification or enhancements_

* e.g. use of one transaction for batch load process will not scale, sufficient while < 1000 customers
* e.g. need to revisit the error handling for scenario X
* e.g. potential to upgrade library Y to take advantage of new features which would enable Z
* e.g. could simplify the code by consolidating the three internal services

