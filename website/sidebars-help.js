module.exports = {
  help: [
    {
      type: 'category',
      label: 'Template Pages',
      items: [
        'getting-started',
        'main-menu',
        'template-basic-page',
        'template-data-table-page',
      ],
    },
    {
      type: 'category',
      label: 'Docusaurus',
      items: [
        'docusaurus/getting-started',
        'docusaurus/create-a-page',
        'docusaurus/create-a-document',
        'docusaurus/create-a-blog-post',
        'docusaurus/markdown-features',
        'docusaurus/thank-you',
      ],
    },
  ],
};
