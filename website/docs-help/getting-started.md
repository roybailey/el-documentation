---
title: Getting Started
slug: /
---

This page will help you to learn about creating pages on this site.


## Creating New Page

### Step 1 : Add file in `docs` folder

e.g.
```markdown
---
title: New Content
---

## Coming Soon...

```

### Step 2 : Add file to `sidebar.js` index

Add to list of `items` in appropriate `sibebar.js`

