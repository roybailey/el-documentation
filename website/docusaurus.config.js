/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Docusaurus Engineering Site',
  tagline: 'Template Engineering Site using Docusaurus',
  url: 'https://roybailey.gitlab.io',
  baseUrl: '/el-documentation/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'roybailey', // Usually your GitHub org/user name.
  projectName: 'el-documentation', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Engineering Site',
      logo: {
        alt: 'Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Engineering',
          position: 'left',
        },
        {
          to: 'docs-backend/',
          activeBasePath: 'docs-backend',
          label: 'Backend',
          position: 'left',
        },
        {
          to: 'docs-frontend/',
          activeBasePath: 'docs-frontend',
          label: 'Front End',
          position: 'left',
        },
        {
          to: 'docs-qa/',
          activeBasePath: 'docs-qa',
          label: 'QA Testing',
          position: 'left',
        },
        {
          to: 'docs-devops/',
          activeBasePath: 'docs-devops',
          label: 'DevOps',
          position: 'left',
        },
        {
          to: 'docs-help/',
          activeBasePath: 'docs-help',
          label: 'Help',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/roybailey/el-documentation',
          label: 'GitLab Home',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'QA',
              to: 'docs-qa',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      /**
       * Main Docs Folder for Engineering in general
       */
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/roybailey/el-documentation/edit/master/website/',
        },
      },
    ],
  ],
  plugins: [
    /**
     * Additional Docs Folders for Engineering Tribes
     */
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'backend',
        path: 'docs-backend',
        routeBasePath: 'docs-backend',
        sidebarPath: require.resolve('./sidebars-backend.js'),
        // ... other options
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'frontend',
        path: 'docs-frontend',
        routeBasePath: 'docs-frontend',
        sidebarPath: require.resolve('./sidebars-frontend.js'),
        // ... other options
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'devops',
        path: 'docs-devops',
        routeBasePath: 'docs-devops',
        sidebarPath: require.resolve('./sidebars-devops.js'),
        // ... other options
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'qa',
        path: 'docs-qa',
        routeBasePath: 'docs-qa',
        sidebarPath: require.resolve('./sidebars-qa.js'),
        // ... other options
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'help',
        path: 'docs-help',
        routeBasePath: 'docs-help',
        sidebarPath: require.resolve('./sidebars-help.js'),
        // ... other options
      },
    ],
  ],
};
